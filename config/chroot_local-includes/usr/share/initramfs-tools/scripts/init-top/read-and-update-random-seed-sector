#!/bin/sh

# We depend on plymouth to have the splash screen shown while this
# scripts runs.
PREREQS="plymouth"

prereqs() { echo "$PREREQS"; }

case "$1" in
prereqs)
  prereqs
  exit 0
  ;;
esac

set -u
set -e

# The plymouth script was run as a dependency and we don't want it to
# fail the next time it's run as a dependency in the init-premount stage,
# so we set SPLASH=false which makes it a no-op.
export SPLASH=false

# Unset $quiet to make log_begin_msg() and log_end_msg() print messages
# to the console, which in turn makes Plymouth write them to
# /var/log/boot.log.
# shellcheck disable=SC2034
quiet=

# Print commands if the "debug=random-seed" boot parameter is set.
grep -qw "debug=random-seed" /proc/cmdline && set -x

. /scripts/functions

if [ -z "${FSUUID:-}" ]; then
  echo "\$FSUUID is unset, probably because the boot medium is an ISO." \
    "Not restoring random seed from LBA 34."
  exit 0
fi

# Wait for system partition
log_begin_msg "Waiting for system partition to become available"
i=0
while true; do
  if [ -b "/dev/disk/by-uuid/${FSUUID}" ]; then
    SYSTEM_PARTITION="$(readlink -f "/dev/disk/by-uuid/${FSUUID}")"
    PARENT_PATH="$(udevadm info --query=property --name="${SYSTEM_PARTITION}" |
      sed -n '/^ID_PATH=/ s/^ID_PATH=// p')"
    PARENT_DEVICE="$(readlink -f "/dev/disk/by-path/${PARENT_PATH}")"
    if [ -b "${PARENT_DEVICE}" ]; then
      break
    fi
  fi

  if [ "$i" -ge 300 ]; then
    log_failure_msg "Reached timeout waiting for system partition." \
      "Not restoring random seed from LBA 34."
    exit 0
  fi

  sleep 0.2
  i="$((i + 1))"
done
log_end_msg

# Check if this is first boot
GUID=$(sgdisk --print "${PARENT_DEVICE}" | \
       sed -n '/^Disk identifier (GUID)/ s/^Disk identifier (GUID): // p')
if [ "${GUID}" = "17B81DA0-8B1E-4269-9C39-FE5C7B9B58A3" ]; then
  FIRST_BOOT=true
fi

log_begin_msg "Restoring random seed from LBA 34"
dd bs=512 skip=34 count=1 status=none if="${PARENT_DEVICE}" of=/dev/urandom
log_end_msg

# We try to obfuscate the number of times Tails has been booted by
# writing a random number of times (1-1000) to the seed during the first
# boot.
if [ -n "${FIRST_BOOT:-}" ]; then
  ITERATIONS=$((1 + $(od -An -N2 -t uI /dev/urandom) % 1000))
  echo "First boot, writing random seed $ITERATIONS times..."
  PLYMOUTH_INIT_MSG="Preparing Tails for first use..."
  plymouth display-message --text="${PLYMOUTH_INIT_MSG}"

  # Debug output for the following loop would be too verbose
  set +x

  start_time=$(date +%s)
  for i in $(seq 1 "${ITERATIONS}"); do
    dd bs=512 seek=34 count=1 status=none if=/dev/urandom of="${PARENT_DEVICE}"
  done
  end_time=$(date +%s)
  echo "Wrote random seed $ITERATIONS times in $((end_time - start_time)) seconds"
  # `plymouth hide-message` doesn't work (#20401)
  plymouth display-message --text=""

  # Restore debug output
  [ "${debug:-}" = "random-seed" ] && set -x
fi
