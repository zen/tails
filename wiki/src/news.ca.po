# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-08 03:39+0000\n"
"PO-Revision-Date: 2024-02-08 16:36+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Content of: <div>
msgid "[[!meta title=\"News\"]]"
msgstr "[[!meta title=\"Notícies\"]]"

#. type: Content of: <p>
msgid "Subscribe to our newsletter to receive the same news by email:"
msgstr ""
"Subscriviu-vos al nostre butlletí per rebre les mateixes notícies per correu "
"electrònic:"

#. type: Content of: <form>
msgid ""
"<input aria-label=\"Email\" name=\"email\" value=\"\"/> <button aria-"
"label=\"Subscribe\" class=\"button\" type=\"submit\">Subscribe</button>"
msgstr ""
"<input aria-label=\"Email\" name=\"email\" value=\"\"/> <button aria-label="
"\"Subscribe\" class=\"button\" type=\"submit\">Subscriu-me</button>"

#. type: Content of: <form><p>
msgid "[[Archive of all news|news/archive]]"
msgstr "[[Arxiu de totes les notícies|news/archive]]"

#.  Inlined news 
#.  Feed buttons used by puppet-tails:manifests/website/rss2email.pp
#. [[!inline pages="page(news/*) and !news/archive and !news/*/* and !news/discussion and (currentlang() or news/report_2* or news/test_*) and tagged(announce)"
#. show="10" feeds="yes" feedonly="yes" feedfile="emails" sort="-meta(date) age -path"]]
#. type: Content of: outside any tag (error?)
msgid ""
"[[!inline pages=\"page(news/*) and !news/archive and !news/*/* and !news/"
"discussion and (currentlang() or news/report_2* or news/test_*)\" "
"show=\"10\" sort=\"-meta(date) age -path\"]]"
msgstr ""
"[[!inline pages=\"page(news/*) and !news/archive and !news/*/* and !news/"
"discussion and (currentlang() or news/report_2* or news/test_*)\" "
"show=\"10\" sort=\"-meta(date) age -path\"]]"

#~ msgid ""
#~ "[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]"
#~ msgstr ""
#~ "[[!meta stylesheet=\"hide-breadcrumbs\" rel=\"stylesheet\" title=\"\"]]"

#~ msgid ""
#~ "[[!inline pages=\"page(news/*) and !news/archive and !news/*/* and !news/"
#~ "discussion and (currentlang() or news/report_2* or news/test_*) and "
#~ "tagged(announce)\" show=\"10\" feeds=\"yes\" feedonly=\"yes\" "
#~ "feedfile=\"emails\" sort=\"-meta(date) age -path\"]]"
#~ msgstr ""
#~ "[[!inline pages=\"page(news/*) and !news/archive and !news/*/* and !news/"
#~ "discussion and (currentlang() or news/report_2* or news/test_*) and "
#~ "tagged(announce)\" show=\"10\" feeds=\"yes\" feedonly=\"yes\" "
#~ "feedfile=\"emails\" sort=\"-meta(date) age -path\"]]"
