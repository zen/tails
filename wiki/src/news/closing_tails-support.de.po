# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-26 12:26+0100\n"
"PO-Revision-Date: 2023-11-10 04:12+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Closing tails-support@boum.org\"]]\n"
msgstr "[[!meta title=\"Schließung tails-support@boum.org\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Wed, 07 Jun 2017 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Wed, 07 Jun 2017 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
msgid ""
"We decided to close tails-support@boum.org, the public mailing list for user "
"support which [was created in 2013](https://tails.net/news/tails-support/)  "
"after closing the forum that we had on this website."
msgstr ""
"Wir haben beschlossen, tails-support@boum.org, die öffentliche Mailingliste "
"für Benutzerunterstützung, die [2013 erstellt wurde](https://tails.net/news/"
"tails-support/) nach der Schließung des Forums nun auch zu schließen."

#. type: Plain text
msgid "The idea behind having a public space for user support was to:"
msgstr ""
"Die Idee hinter einem öffentlichen Bereich für Benutzerunterstützung war:"

#. type: Bullet: '- '
#, fuzzy
#| msgid ""
#| "Reduce the amount of work for our help desk behind [[tails-support-"
#| "private@boum.org|about/contact#tails-support-private]] (which is "
#| "encrypted and also receives [[*WhisperBack* reports|doc/first_steps/"
#| "bug_reporting]])."
msgid ""
"Reduce the amount of work for our help desk behind [[tails-support-"
"private@boum.org|about/contact#tails-support-private]] (which is encrypted "
"and also receives [[*WhisperBack* reports|doc/first_steps/whisperback]])."
msgstr ""
"Reduktion des Arbeitsaufwands für unseren Helpdesk von [[tails-support-"
"private@boum.org|about/contact#tails-support-private]] (der verschlüsselt "
"ist und auch [[*WhisperBack* reports|doc/first_steps/bug_reporting]]) "
"empfängt."

#. type: Plain text
#, no-wrap
msgid ""
"  This is not really working as tails-support-private@boum.org still\n"
"  has 12 times more traffic than tails-support@boum.org.\n"
msgstr ""
"  Dies funktioniert nicht wirklich als tails-support-private@boum.org\n"
"  hat 12 mal mehr Datenaufkommen als tails-support@boum.org.\n"

#. type: Plain text
msgid "- Build a community of people doing user support outside of our team."
msgstr ""
"- Aufbau einer Community von Personen, die außerhalb unseres Teams "
"Benutzerunterstützung leisten."

#. type: Plain text
#, no-wrap
msgid ""
"  This is not really working either as most of the threads are still\n"
"  answered by our help desk or other core\n"
"  contributors.\n"
msgstr ""
"  Dies funktioniert auch nicht wirklich, da die meisten Threads noch\n"
"  vorhanden sind beantwortet von unserem Helpdesk oder einem\n"
"  anderen Kern aus Mitwirkenden.\n"

#. type: Plain text
msgid ""
"- Build a public database to make it easy to consult or reuse previous "
"answers."
msgstr ""
"- Erstellung einer öffentlichen Datenbank, um frühere Antworten einfach "
"abrufen oder wiederverwenden zu können."

#. type: Plain text
#, no-wrap
msgid ""
"  This is not really working either as the archive of the mailing list\n"
"  are hard to search and reuse and it's easier to point people to\n"
"  the documentation, known issues, or FAQ.\n"
msgstr ""
"  Dies funktioniert auch nicht wirklich als Archiv der Mailingliste\n"
"  sind schwer zu suchen und wiederzuverwenden und es ist einfacher, Leute darauf hinzuweisen\n"
"  die Dokumentation, bekannte Probleme oder FAQ zu lesen.\n"

#. type: Plain text
msgid "Having a public mailing list also makes it:"
msgstr "Eine öffentliche Mailingliste:"

#. type: Bullet: '- '
msgid ""
"Complicated to make sure that both the list and the sender receive our "
"answers."
msgstr ""
"Kompliziert sicherzustellen, dass sowohl die Liste als auch der Absender "
"unsere Antworten erhalten."

#. type: Bullet: '- '
msgid ""
"Sensitive to ask for more technical information about the problem, such as "
"logs, as they are harder for the person reporting the error to share with us "
"and also raises privacy concerns."
msgstr ""
"Es ist sensibel, weitere technische Informationen zu dem Problem "
"anzufordern, z.B. Protokolle, da diese für die Person, die den Fehler "
"meldet, schwieriger mit uns zu teilen sind und auch Datenschutzbedenken "
"aufwerfen."

#. type: Bullet: '- '
msgid ""
"Duplicate work between tails-support-private@boum.org and tails-support@boum."
"org as people often write to both."
msgstr ""
"Doppelte Arbeit zwischen tails-support-private@boum.org und tails-"
"support@boum.org, da häufig an beide Adressen geschrieben wird."

#. type: Plain text
msgid ""
"The major downside of closing tails-support@boum.org is that it was a good "
"place for developers to have some feedback on the recurrent problems faced "
"by users. We want to solve this while working on a [request tracker for our "
"help desk](https://tails.net/blueprint/RT_for_help_desk/)."
msgstr ""

#. type: Plain text
msgid ""
"See you on [[tails-support-private@boum.org|about/contact#tails-support-"
"private]]!"
msgstr ""
"Wir sehen uns auf [[tails-support-private@boum.org|Über/Kontakt#tails-"
"support-private]]!"
