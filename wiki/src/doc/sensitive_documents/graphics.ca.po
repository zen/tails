# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-09-04 09:56+0200\n"
"PO-Revision-Date: 2023-11-13 23:11+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Graphics\"]]\n"
msgstr "[[!meta title=\"Gràfics\"]]\n"

#. type: Plain text
msgid ""
"Tails includes several applications for image manipulation, graphic design, "
"and page layout:"
msgstr ""
"Tails inclou diverses aplicacions per a la manipulació d'imatges, disseny "
"gràfic i disseny de pàgina:"

#. type: Bullet: '  - '
msgid "[*GIMP*](https://www.gimp.org/), an image editor."
msgstr "[*GIMP*](https://www.gimp.org/), un editor d'imatges."

#. type: Plain text
#, no-wrap
msgid ""
"    You can use *GIMP* to edit, enhance, and retouch photos and scans, create\n"
"    drawings, and make your own images.\n"
msgstr ""
"    Podeu utilitzar *GIMP* per editar, millorar i retocar fotos i escanejos, "
"crear\n"
"    dibuixos i fer les vostres pròpies imatges.\n"

#. type: Plain text
#, no-wrap
msgid ""
"    *GIMP* has [user manuals](https://www.gimp.org/docs/) in several\n"
"    languages.\n"
msgstr ""
"    *GIMP* té [manuals d'usuari](https://www.gimp.org/docs/) en diverses\n"
"    llengües.\n"

#. type: Bullet: '  - '
msgid ""
"[*Inkscape*](https://inkscape.org/), a vector-based drawing application."
msgstr ""
"[*Inkscape*](https://inkscape.org/), una aplicació de dibuix basada en "
"vectors."

#. type: Plain text
#, no-wrap
msgid ""
"    You can use *Inkscape* to create a wide variety of graphics such as\n"
"    illustrations, icons, logos, diagrams, maps, and web graphics.\n"
msgstr ""
"    Podeu utilitzar *Inkscape* per crear una gran varietat de gràfics com "
"ara\n"
"    il·lustracions, icones, logotips, diagrames, mapes i gràfics web.\n"

#. type: Plain text
#, no-wrap
msgid ""
"    The *Inkscape* website has\n"
"    [tutorials](https://inkscape.org/en/learn/tutorials/) in several languages.\n"
msgstr ""
"    El lloc web d'*Inkscape* té\n"
"    [tutorials](https://inkscape.org/en/learn/tutorials/) en diverses "
"llengües.\n"

#. type: Plain text
#, no-wrap
msgid ""
"These applications can be started from the\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Graphics</span></span> menu.\n"
msgstr ""
"Aquestes aplicacions es poden iniciar des del menú\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Aplicacions</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Gràfics</span></span>.\n"

#. type: Plain text
msgid ""
"You can also install using the [[Additional Software|persistent_storage/"
"additional_software]] feature:"
msgstr ""
"També podeu instal·lar-lo mitjançant la funció [[Programari Addicional|"
"persistent_storage/additional_software]]:"

#. type: Plain text
msgid "- **[*Scribus*](https://www.scribus.net/)**, a page layout application."
msgstr ""
"- **[*Scribus*](https://www.scribus.net/)**, una aplicació de disseny de "
"pàgina."

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents/persistence\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""
"[[!inline pages=\"doc/sensitive_documents/persistence.ca\" raw=\"yes\" sort="
"\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/sensitive_documents/metadata.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/sensitive_documents/metadata.inline.ca\" raw=\"yes\" sort=\"age\"]]\n"
