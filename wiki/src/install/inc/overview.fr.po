# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2024-02-08 22:01+0000\n"
"PO-Revision-Date: 2024-05-22 18:50+0000\n"
"Last-Translator: xin <xin@riseup.net>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 5.3\n"

#. type: Content of: outside any tag (error?)
msgid "[[!meta robots=\"noindex\"]]"
msgstr "[[!meta robots=\"noindex\"]]"

#. type: Content of: <h2>
msgid "You need"
msgstr "Vous avez besoin de"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/usb.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/usb.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "1 USB stick"
msgstr "1 clé USB"

#. type: Content of: <div><div><p>
msgid "Only for Tails!"
msgstr "Seulement pour Tails !"

#. type: Content of: <div><div><p>
msgid "8 GB minimum"
msgstr "d'au moins 8 Go"

#. type: Content of: <div><div><p>
msgid "Your Tails"
msgstr "Votre Tails"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/intermediary.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/intermediary.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "All data will be lost!"
msgstr "Toutes les données seront perdues !"

#. type: Content of: <div><div><p>
msgid "[[!toggle id=\"why_extra\" text=\"Why?\"]]"
msgstr "[[!toggle id=\"why_extra\" text=\"Pourquoi ?\"]]"

#. type: Content of: <div><div><div>
msgid ""
"[[!toggleable id=\"why_extra\" text=\"\"\" [[!toggle id=\"why_extra\" "
"text=\"\"]]"
msgstr ""
"[[!toggleable id=\"why_extra\" text=\"\"\" [[!toggle id=\"why_extra\" "
"text=\"\"]]"

#. type: Content of: <div><div><div><p>
msgid ""
"It is currently impossible to manually upgrade a Tails USB stick while "
"running from itself. This scenario requires creating an intermediary Tails "
"on another USB stick, from which to upgrade your Tails."
msgstr ""
"Il est actuellement impossible de faire une mise à jour manuelle d'une clé "
"USB Tails pendant son fonctionnement. Ce scénario nécessite de créer un "
"Tails intermédiaire sur une autre clé USB, depuis laquelle vous pourrez "
"mettre à jour votre Tails."

#. type: Content of: <div><div><div>
msgid "\"\"\"]]"
msgstr "\"\"\"]]"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/windows.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/windows.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "Windows 7"
msgstr "Windows 7"

#. type: Content of: <div><div><p>
msgid "or later"
msgstr "ou plus récent"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/apple.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/apple.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "macOS 10.10"
msgstr "macOS 10.10"

#. type: Content of: <div><div><p>
msgid "Yosemite"
msgstr "Yosemite"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/linux.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/linux.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "Linux"
msgstr "Linux"

#. type: Content of: <div><div><p>
msgid "any distribution"
msgstr "n'importe quelle distribution"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/debian.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/debian.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "Debian"
msgstr "Debian"

#. type: Content of: <div><div><p>
msgid "Ubuntu"
msgstr "Ubuntu"

#. type: Content of: <div><div><p>
msgid "or another derivative"
msgstr "ou une autre dérivée"

#. type: Content of: <div><div><p>
msgid "Another Tails"
msgstr "Un autre Tails"

#. type: Content of: <div><div><p>
msgid "USB stick or DVD"
msgstr "sur une clé USB ou un DVD"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/laptop.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/laptop.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "2 GB of RAM"
msgstr "2 Go de RAM"

#. type: Content of: <div><div><p>
msgid ""
"<a href=\"https://support.microsoft.com/en-us/windows/32-bit-and-64-bit-"
"windows-frequently-asked-questions-c6ca9541-8dce-4d48-0415-94a3faa2e13d\">64-"
"bit</a>"
msgstr ""
"<a href=\"https://support.microsoft.com/fr-fr/windows/32-bit-and-64-bit-"
"windows-frequently-asked-questions-c6ca9541-8dce-4d48-0415-94a3faa2e13d\">64 "
"bits</a>"

#. type: Content of: <div><div><p>
msgid "Intel processor"
msgstr "Processeur Intel"

#. type: Content of: <div><div><p>
msgid "not Apple M1 or M2"
msgstr "pas Apple M1 ou M2"

#. type: Content of: <div><div><p>
msgid ""
"<a href=\"https://www.howtogeek.com/198615/how-to-check-if-your-linux-system-"
"is-32-bit-or-64-bit/\">64-bit</a>"
msgstr ""
"<a href=\"https://www.howtogeek.com/198615/"
"how-to-check-if-your-linux-system-is-32-bit-or-64-bit/\">64 bits</a>"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/phone.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/phone.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "Smartphone"
msgstr "Smartphone"

#. type: Content of: <div><div><p>
msgid "or printer"
msgstr "ou imprimante"

#. type: Content of: <div><div><p>
msgid "to follow the instructions"
msgstr "pour suivre les instructions"

#. type: Content of: <div><div>
msgid "[[!img install/inc/icons/clock.png link=\"no\" alt=\"\"]]"
msgstr "[[!img install/inc/icons/clock.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><div><p>
msgid "1 hour in total"
msgstr "1 heure au total"

#. type: Content of: <div><div><p>
msgid "½ hour"
msgstr "½ heure"

#. type: Content of: <div><div><p>
msgid "¼ hour"
msgstr "¼ heure"

#. type: Content of: <div><div><p>
msgid ""
"[[!inline pages=\"inc/stable_amd64_iso_size\" raw=\"yes\" sort=\"age\"]] to "
"download"
msgstr ""
"[[!inline pages=\"inc/stable_amd64_iso_size\" raw=\"yes\" sort=\"age\"]] à "
"télécharger"

#. type: Content of: <div><div><p>
msgid "½ hour to install"
msgstr "½ heure pour installer"

#. type: Content of: <div><div><p>
msgid "½ hour to upgrade"
msgstr "½ heure pour mettre à jour"

#. type: Content of: <p>
msgid ""
"[[!toggle id=\"requirements\" text=\"Detailed system requirements and "
"recommended hardware.\"]]"
msgstr ""
"[[!toggle id=\"requirements\" text=\"Configuration requise détaillée et "
"matériel recommandé.\"]]"

#. type: Content of: outside any tag (error?)
msgid ""
"[[!toggleable id=\"requirements\" text=\"\"\" <span class=\"hide\">[[!toggle "
"id=\"requirements\" text=\"\"]]</span> [[!inline pages=\"doc/about/"
"requirements\" raw=\"yes\" sort=\"age\"]] \"\"\"]]"
msgstr ""
"[[!toggleable id=\"requirements\" text=\"\"\" <span class=\"hide\">[[!toggle "
"id=\"requirements\" text=\"\"]]</span> [[!inline pages=\"doc/about/"
"requirements.fr\" raw=\"yes\" sort=\"age\"]] \"\"\"]]"

#. type: Content of: <h2>
msgid "Your steps"
msgstr "Votre marche à suivre"

#. type: Content of: <div><a>
msgid ""
"<a href=\"#download\" class=\"windows linux mac expert\"> [[!img install/inc/"
"infography/download.png link=\"no\" alt=\"\"]]"
msgstr ""
"<a href=\"#download\" class=\"windows linux mac expert\"> [[!img install/inc/"
"infography/download.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Download Tails"
msgstr "Télécharger Tails"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#download\" class=\"upgrade-os\"> [[!img install/inc/"
"infography/download-with-intermediary.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#download\" class=\"upgrade-os\"> [[!img install/inc/"
"infography/download-with-intermediary.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#download\" class=\"upgrade-tails\"> [[!img install/inc/"
"infography/download-from-tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#download\" class=\"upgrade-tails\"> [[!img install/inc/"
"infography/download-from-tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Download the upgrade"
msgstr "Télécharger la mise à jour"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#verify\" class=\"windows linux mac expert\"> [[!img install/"
"inc/infography/verify.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#verify\" class=\"windows linux mac expert\"> [[!img install/"
"inc/infography/verify.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Verify your download"
msgstr "Vérifier votre téléchargement"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#verify\" class=\"upgrade-os\"> [[!img install/inc/infography/"
"verify-with-intermediary.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#verify\" class=\"upgrade-os\"> [[!img install/inc/infography/"
"verify-with-intermediary.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#verify\" class=\"upgrade-tails\"> [[!img install/inc/"
"infography/verify-from-tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#verify\" class=\"upgrade-tails\"> [[!img install/inc/"
"infography/verify-from-tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#other\" class=\"pc-clone mac-clone upgrade-clone\"> [[!img "
"install/inc/infography/restart-on-other-tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#other\" class=\"pc-clone mac-clone upgrade-clone\"> [[!img "
"install/inc/infography/restart-on-other-tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Restart on the other Tails"
msgstr "Redémarrer sur l'autre Tails"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#etcher\" class=\"windows mac\"> [[!img install/inc/"
"infography/install-etcher.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#etcher\" class=\"windows mac\"> [[!img install/inc/"
"infography/install-etcher.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Download <i>balenaEtcher</i>"
msgstr "Télécharger <i>balenaEtcher</i>"

#. type: Content of: <div><a><p>
msgid "Install <i>balenaEtcher</i>"
msgstr "Installer <i>balenaEtcher</i>"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#etcher\" class=\"upgrade-windows upgrade-mac\"> [[!img "
"install/inc/infography/install-etcher-with-intermediary.png link=\"no\" "
"alt=\"\"]]"
msgstr ""
"</a> <a href=\"#etcher\" class=\"upgrade-windows upgrade-mac\"> [[!img "
"install/inc/infography/install-etcher-with-intermediary.png link=\"no\" alt="
"\"\"]]"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#install\" class=\"windows linux mac expert\"> [[!img install/"
"inc/infography/install-tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#install\" class=\"windows linux mac expert\"> [[!img install/"
"inc/infography/install-tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Install Tails using <i>balenaEtcher</i>"
msgstr "Installer Tails à l'aide de <i>balenaEtcher</i>"

#. type: Content of: <div><a><p>
msgid "Install Tails using <i>GNOME Disks</i>"
msgstr "Installer Tails à l'aide de <i>GNOME Disks</i>"

#. type: Content of: <div><a><p>
msgid "Install Tails using <code>dd</code>"
msgstr "Installer Tails à l'aide de <code>dd</code>"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#install\" class=\"upgrade-mac upgrade-windows\"> [[!img "
"install/inc/infography/install-upgrade-usb-only.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#install\" class=\"upgrade-mac upgrade-windows\"> [[!img "
"install/inc/infography/install-upgrade-usb-only.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Install intermediary using <i>balenaEtcher</i>"
msgstr "Installer l'intermédiaire à l'aide de <i>balenaEtcher</i>"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#install\" class=\"upgrade-linux\"> [[!img install/inc/"
"infography/install-upgrade-usb-only.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#install\" class=\"upgrade-linux\"> [[!img install/inc/"
"infography/install-upgrade-usb-only.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Install intermediary using <i>GNOME Disks</i>"
msgstr "Installer l'intermédiaire à l'aide de <i>GNOME Disks</i>"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#install\" class=\"upgrade-tails\"> [[!img install/inc/"
"infography/install-upgrade-usb.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#install\" class=\"upgrade-tails\"> [[!img install/inc/"
"infography/install-upgrade-usb.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#restart\" class=\"windows linux mac expert\"> [[!img install/"
"inc/infography/restart.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#restart\" class=\"windows linux mac expert\"> [[!img install/"
"inc/infography/restart.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Restart on your Tails USB stick"
msgstr "Redémarrer sur votre clé USB Tails"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#intermediary\" class=\"upgrade-os\"> [[!img install/inc/"
"infography/restart-on-other-tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#intermediary\" class=\"upgrade-os\"> [[!img install/inc/"
"infography/restart-on-other-tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Restart on intermediary"
msgstr "Redémarrer sur l'intermédiaire"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#intermediary\" class=\"upgrade-tails\"> [[!img install/inc/"
"infography/restart-on-upgrade-usb.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#intermediary\" class=\"upgrade-tails\"> [[!img install/inc/"
"infography/restart-on-upgrade-usb.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#install\" class=\"pc-clone mac-clone\"> [[!img install/inc/"
"infography/clone-tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#install\" class=\"pc-clone mac-clone\"> [[!img install/inc/"
"infography/clone-tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Install Tails by cloning"
msgstr "Installer Tails par clonage"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#upgrade\" class=\"upgrade-tails upgrade-os upgrade-clone\"> "
"[[!img install/inc/infography/clone-tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#upgrade\" class=\"upgrade-tails upgrade-os upgrade-clone\"> "
"[[!img install/inc/infography/clone-tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Upgrade your Tails by cloning"
msgstr "Mettre à jour votre Tails par clonage"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#restart-new\" class=\"pc-clone mac-clone\"> [[!img install/"
"inc/infography/restart-on-new-tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#restart-new\" class=\"pc-clone mac-clone\"> [[!img install/"
"inc/infography/restart-on-new-tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Restart on your new Tails"
msgstr "Redémarrer sur votre nouveau Tails"

#. type: Content of: <div><a>
msgid ""
"</a> <a href=\"#tails\" class=\"windows linux mac expert\"> [[!img install/"
"inc/infography/tails.png link=\"no\" alt=\"\"]]"
msgstr ""
"</a> <a href=\"#tails\" class=\"windows linux mac expert\"> [[!img install/"
"inc/infography/tails.png link=\"no\" alt=\"\"]]"

#. type: Content of: <div><a><p>
msgid "Welcome to Tails!"
msgstr "Bienvenue dans Tails !"

#. type: Content of: <div>
msgid "</a>"
msgstr "</a>"

#~ msgid ""
#~ "</a> <a href=\"#new\" class=\"pc-clone mac-clone\"> [[!img install/inc/"
#~ "infography/tails.png link=\"no\" alt=\"\"]]"
#~ msgstr ""
#~ "</a> <a href=\"#new\" class=\"pc-clone mac-clone\"> [[!img install/inc/"
#~ "infography/tails.png link=\"no\" alt=\"\"]]"

#~ msgid "Welcome to your new Tails!"
#~ msgstr "Bienvenue dans votre nouveau Tails !"

#~ msgid "[[!img install/inc/icons/tails.png link=\"no\" alt=\"\"]]"
#~ msgstr "[[!img install/inc/icons/tails.png link=\"no\" alt=\"\"]]"

#~ msgid "Install Tails using <span class=\"command\">dd</span>"
#~ msgstr "Installation de Tails à l'aide de <span class=\"command\">dd</span>"

#~ msgid "[[!img install/inc/icons/screens.png link=\"no\" alt=\"\"]]"
#~ msgstr "[[!img install/inc/icons/screens.png link=\"no\" alt=\"\"]]"

#~ msgid "another computer,"
#~ msgstr "un autre ordinateur,"

#~ msgid ""
#~ "<span class=\"windows linux mac expert upgrade-tails upgrade-os\">1 hour "
#~ "in total</span> <span class=\"install-clone mac-clone\">½ hour</span> "
#~ "<span class=\"upgrade-clone\">¼ hour</span>"
#~ msgstr ""
#~ "<span class=\"windows linux mac expert upgrade-tails upgrade-os\">1 heure "
#~ "au total</span> <span class=\"install-clone mac-clone\">½ heure</span> "
#~ "<span class=\"upgrade-clone\">¼ d'heure</span>"

#~ msgid ""
#~ "</span> <span class=\"windows linux mac expert\"><small>½ hour to "
#~ "install</small></span> <span class=\"upgrade-tails upgrade-os\"><small>½ "
#~ "hour to upgrade</small></span>"
#~ msgstr ""
#~ "</span> <span class=\"windows linux mac expert\"><small>½ heure pour "
#~ "l'installation</small></span> <span class=\"upgrade-tails upgrade-"
#~ "os\"><small>½ heure pour la mise à jour</small></span>"

#~ msgid "[[!img install/inc/icons/usb.png link=\"no\"]]"
#~ msgstr "[[!img install/inc/icons/usb.png link=\"no\"]]"

#~ msgid "<small>All data will be lost!</small>"
#~ msgstr "<small>Toutes les données seront perdues !</small>"

#~ msgid ""
#~ "[[!img install/inc/infography/download.png link=\"no\" alt=\"USB image "
#~ "downloaded\"]]"
#~ msgstr ""
#~ "[[!img install/inc/infography/download.png link=\"no\" alt=\"Image USB "
#~ "téléchargée\"]]"

#~ msgid ""
#~ "[[!img install/inc/infography/verify.png link=\"no\" alt=\"USB image "
#~ "verified\"]]"
#~ msgstr ""
#~ "[[!img install/inc/infography/verify.png link=\"no\" alt=\"Image USB "
#~ "vérifiée\"]]"

#~ msgid "Verify"
#~ msgstr "Vérification"

#~ msgid ""
#~ "[[!img install/inc/infography/restart-on-other-tails.png link=\"no\" "
#~ "alt=\"Computer restarted on USB stick on the left\"]]"
#~ msgstr ""
#~ "[[!img install/inc/infography/restart-on-other-tails.png link=\"no\" "
#~ "alt=\"Ordinateur redémarré sur la clé USB de gauche\"]]"

#~ msgid "Restart"
#~ msgstr "Redémarrage"

#~ msgid ""
#~ "[[!img install/inc/infography/install-etcher.png link=\"no\" "
#~ "alt=\"balenaEtcher downloaded\"]]"
#~ msgstr ""
#~ "[[!img install/inc/infography/install-etcher.png link=\"no\" "
#~ "alt=\"balenaEtcher téléchargé\"]]"

#~ msgid ""
#~ "[[!img install/inc/infography/install-tails.png link=\"no\" alt=\"USB "
#~ "image installed on USB stick\"]]"
#~ msgstr ""
#~ "[[!img install/inc/infography/install-tails.png link=\"no\" alt=\"Image "
#~ "USB installée sur la clé USB\"]]"

#~ msgid ""
#~ "[[!img install/inc/infography/install-upgrade-usb.png link=\"no\" "
#~ "alt=\"USB image installed on USB stick on the left\"]]"
#~ msgstr ""
#~ "[[!img install/inc/infography/install-upgrade-usb.png link=\"no\" "
#~ "alt=\"Image USB installée sur la clé USB de gauche\"]]"

#~ msgid "Install intermediary"
#~ msgstr "Installation intermédiaire"

#~ msgid ""
#~ "[[!img install/inc/infography/clone-tails.png link=\"no\" alt=\"Tails "
#~ "installed on USB stick on the right\"]]"
#~ msgstr ""
#~ "[[!img install/inc/infography/clone-tails.png link=\"no\" alt=\"Tails "
#~ "installé sur la clé USB de droite\"]]"

#~ msgid ""
#~ "<span class=\"install-clone mac-clone\">Install Tails</span> <span "
#~ "class=\"upgrade\">Upgrade Tails</span>"
#~ msgstr ""
#~ "<span class=\"install-clone mac-clone\">Installation de Tails</span> "
#~ "<span class=\"upgrade\">Mise à jour de Tails</span>"

#~ msgid "Tails"
#~ msgstr "Tails"

#~ msgid ""
#~ "[[!img install/inc/infography/create-persistence.png link=\"no\" "
#~ "alt=\"Encryption configured on USB stick\"]]"
#~ msgstr ""
#~ "[[!img install/inc/infography/create-persistence.png link=\"no\" "
#~ "alt=\"Chiffrement configuré sur la clé USB\"]]"

#~ msgid "Configure"
#~ msgstr "Configuration"
